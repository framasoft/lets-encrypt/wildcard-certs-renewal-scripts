# Scripts to renew LE certificates using DNS challenge and Gandi API

## How to use

Clone the repository:
```
cd /opt
git clone https://framagit.org/framasoft/lets-encrypt/wildcard-certs-renewal-scripts
```

Create a file name `gandi_api_key` containing your Gandi API key in the directory:
```
echo "your_key" > /opt/wildcard-certs-renewal-scripts/gandi_api_key
chmod 600 wildcard-certs-renewal-scripts/gandi_api_key
sudo chown root: wildcard-certs-renewal-scripts/gandi_api_key
```

To create the certificate:
```
certbot -d *.example.org \
  --email tech@example.org \
  --agree-tos \
  --text \
  --renew-hook "/usr/sbin/nginx -s reload" \
  --manual \
  --no-eff-email \
  --preferred-challenges dns-01 \
  --server https://acme-v02.api.letsencrypt.org/directory \
  --manual-auth-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_auth.sh \
  --manual-cleanup-hook /opt/wildcard-certs-renewal-scripts/certbot_wildcard_cleanup.sh \
  certonly
```

## License

Released under the terms of the GNU G.P.L. v3.0. See [LICENSE](LICENSE) file.
