#!/bin/bash
# Docs:
# - certbot: https://certbot.eff.org/docs/using.html#hooks
# - http://doc.livedns.gandi.net/#work-with-domains

function join_by {
    local IFS="$1";
    shift;
    echo "$*";
}

my_array=($(echo $CERTBOT_DOMAIN | tr "." "\n"))
DOMAIN=$CERTBOT_DOMAIN
SUB_DOMAIN=_acme-challenge
if [[ ${#my_array[@]} -gt 2 ]]
then
    DOMAIN=$(join_by '.' "${my_array[@]: -2}")
    SUB_DOMAIN=$(join_by '.' _acme-challenge "${my_array[@]:0:(( ${#my_array[@]} -2 ))}")
fi

curl \
 -s \
 -X DELETE \
 -H "Content-Type: application/json" \
 -H "Authorization: Apikey $(cat /opt/wildcard-certs-renewal-scripts/gandi_api_key)" \
 https://api.gandi.net/v5/livedns/domains/$DOMAIN/records/$SUB_DOMAIN
